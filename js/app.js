$(document).foundation()
var value = 0;
var slidesCount = $('.slider li').length;

$(document).ready(function(){
   $('.slider').css({
     width: (slidesCount*100) + '%'
   });
   $('.slider li').css({
     width: (100/slidesCount) + '%'
   });
 });

$(".button-next").click(function() {
    value += $(window).width();
    if (value > $(window).width() * (slidesCount - 1)) {
        value = 0;
    }
    $('.slider').animate({
        right: value
    }, 1000);
    $(this).blur();
});

$(".button-prev").click(function() {
    value -= $(window).width();
    if (value < 0) {
        value = $(window).width() * (slidesCount - 1);
    }
    $('.slider').animate({
        right: value
    }, 1000);
    $(this).blur();
});
